var exports = module.exports = {};
var motebus = require('motebus');
var xrpc = motebus.xRPC();
var xmsg = motebus.xMsg();

//--------------------------------
exports.xCall = async function(MMA, Fname, Dobj, Prio, T1, T2) {
    return new Promise(function(resolve, reject){
        xrpc.call( MMA, Fname, Dobj, Prio, T1,T2)

		.then((Result)=>{
			console.log("xCall Result:", Result);
            resolve(Result);
		})
		.catch((Err)=>{
			console.log("xCall Result:", Err);
            reject(Err);
		});
    })
     
}



exports.xSend = function(MMA, Body, Files, Prio, T1, T2) {

		xmsg.send(MMA, Body, Files, Prio, T1, T2, (err, tkinfo) => {
			if (err) {
				console.error(err);
			} else {
				if (tkinfo.state != 'Reply') {
					console.log("  > tkinfo(send) id=%s, state= %s", tkinfo.id, tkinfo.state);	
				} else {
					console.log("  > tkinfo(send) id=%s, state= %s, body=", 
						tkinfo.id, tkinfo.state, tkinfo.msg.body, ", files=", tkinfo.msg.files);
				}
			}
		});
}


exports.xReply = function(Msg, Body, Files, Prio, T1, T2) {

		xmsg.reply(Msg, Body, Files, Prio, T1, T2, (err, tkinfo) => {
			if (err) {
				console.error(err);
			} else {
				console.log("  > tkinfo(Reply) id=%s state= %s", tkinfo.id, tkinfo.state);	
			}
		});
}





exports.xOpen = function(AppName) {

		xmsg.open(AppName, '', false, (err, result) =>{

		});
}


exports.xExtract = function(Msg, outpath) {
		xmsg.extract( Msg.head.id, outpath, (err,result) => {
				if (err) {
					console.error(err);
				} else {
					console.log("xmsg.extract(id=%s) result= %s", Msg.head.id, result);	
				}
			});
}