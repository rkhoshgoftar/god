

function success(data){
    return {
        RstCode : "0",
        RstMsg :  "OK",
        Data: data
    };
}

function error(msg, statusCode){
    return {
        RstCode : statusCode,
        RstMsg: msg
    };
}


module.exports = {
    success,
    error
}