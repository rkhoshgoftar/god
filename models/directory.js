const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const directorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    insertDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updateDate: {
        type: Date,
        required:false
    },
    properties: {},
    groups:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Group'
    }]
});

const Directory = mongoose.model('Directory', directorySchema);

function validateDirectory(param){
    const schema = {
        name: Joi.string().required().min(3)
    }

    return Joi.validate(param, schema);
}

exports.directorySchema = directorySchema;
exports.Directory = Directory;
exports.validate = validateDirectory;

