const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const  materializedPlugin = require('mongoose-materialized');

const groupSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        
    },
    ddn: {
        type: String,
        require: false
    },
    uid: {
        type: String,
        require: false
    },
    insertDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updateDate: {
        type: Date,
        required:false
    },
    properties: {},
    objects:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Object'
    }]
});

groupSchema.method('transform', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.gid = obj._id;
    delete obj._id;
 
    return obj;
});

groupSchema.plugin(materializedPlugin);

const Group = mongoose.model('Group', groupSchema);
Group.Building(function(){
    // building materialized path
});

function validateGroup(param){
    const schema = {
        name: Joi.string().required().min(3)
    }

    return Joi.validate(param, schema);
}

exports.groupSchema = groupSchema;
exports.Group = Group;
exports.validate = validateGroup;

