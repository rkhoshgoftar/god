const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const objectSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    ddn: {
        type: String,
        require: false
    },
    uid: {
        type: String,
        require: false
    },
    insertDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updateDate: {
        type: Date,
        required:false
    },
    properties: {}   
});

groupSchema.method('transform', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.oid = obj._id;
    delete obj._id;
 
    return obj;
});

const Objects = mongoose.model('Object', objectSchema);

function validateObject(param){
    const schema = {
        name: Joi.string().required().min(3)
    }

    return Joi.validate(param, schema);
}

exports.objectSchema = objectSchema;
exports.Objects = Objects;
exports.validateObject = validateObject;

