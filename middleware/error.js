const winston = require('winston');
require('winston-mongodb');
const logger = winston.createLogger({
    transports: [
        new winston.transports.MongoDB({
            db: 'mongodb://localhost:27017/god',
            collection: 'log',
            level: 'info',
            storeHost: true,
            capped: true,
        })
    ]
});
module.exports = function(err, req, res, next){
    if(err)
        logger.error(err.message, err);
    res.status(500).send('Something failed.');
}