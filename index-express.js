const winston = require('winston');
require('winston-mongodb');
require('express-async-errors');
const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const app = express(feathers());
var config = require('config');
const logger = winston.createLogger({
  transports: [
      new winston.transports.MongoDB({
          db: config.get('db'),
          collection: 'log',
          storeHost: true,
          capped: true,
      })
  ]
});

require('./startup/logging')(logger);
require('./startup/db')(logger);
require('./startup/config')(app, express);
require('./startup/routes')(app, express);


// Start the server
app.listen(3030).on('listening', () =>
  winston.info('Feathers server listening on localhost:3030')
);