
var motebus = require('motebus');
var os = require('os');

console.log('OS: ',os.platform());
var isWin32 = os.platform() == 'win32';

var testReply = true;
var testWithFiles = false;
var gsn = 0;

motebus.on('ready', ()=>{
	gsn++;
	console.log("Event: motebus.ready()\n");


	// ================= Host Management API Demo =======================
	// Get local info
	motebus.getInfo()
	.then((result)=>{
		console.log("motebus.getInfo() result: \n", result, "\n");
	})
	.catch((err)=>{
		console.log("motebus.getInfo() error: \n", err, "\n");
	});


/*
	// Cluster mode
	//   hostCluster(target, serverlist, smode)
	//     smode = 0-Random, 1-Linear, 2-RoundRobin
	var serverlist = ["192.168.0.100:6780","192.168.0.54:6780","192.168.0.102:6780"];
	motebus.hostCluster("dc", serverlist, 0)
	.then((result)=>{
		console.log("motebus.hostLink() result:\n", result, "\n");
	})
	.catch((err)=>{
		console.log("motebus.hostLink() error: \n", err, "\n");
	});
*/

	
/*
	// List all host
	motebus.getHostList()
	.then((result)=>{
		console.log("motebus.getHostList() result:\n", result, "\n");
	})
	.catch((err)=>{
		console.log("motebus.getHostList() error: \n", err, "\n");
	});
*/

/*
	// Predefine host link
	//   hostLink( target, conCount, retries )
	motebus.hostLink("192.168.1.27:6780", 1, 10)
	.then((result)=>{
		console.log("motebus.hostLink() result:\n", result, "\n");
	})
	.catch((err)=>{
		console.log("motebus.hostLink() error: \n", err, "\n");
	});
*/

/*
	// Remove host link
    //   hostUnlink( target )
	motebus.hostUnlink("192.168.1.12:6780")
	.then((result)=>{
		console.log("motebus.hostUnlink() result:\n", result, "\n");
	})
	.catch((err)=>{
		console.log("motebus.hostUnlink() error: \n", err, "\n");
	});
*/



	// ================= xMsg API Demo =======================
	var xmsg = motebus.xMsg();
	xmsg.open('echo', '', false, (err, result) =>{
		//var result = false;
		if (result) {

			// Receive message
			xmsg.on('message', (msg) => {
				console.log("Event: xmsg.message( head=", msg.head, ", body=", msg.body, ", files=", msg.files, " )\n" );

				// Extract files
				if (testWithFiles) {
					var outpath;
					if (isWin32)
						outpath = "D:\\Project\\VOIP\\MoteBus\\Runtime\\Temp";
					else
						outpath = "/home/richard/Project/MoteBus/Runtime/Temp";

					xmsg.extract( msg.head.id, outpath, (err,result) => {
						if (err) {
							console.error(err);
						} else {
							console.log("xmsg.extract(id=%s) result= %s", msg.head.id, result);	
						}
					});
				}

				// Reply message
				if (testReply) {
					console.log('< Reply message');
					var body = {"text": "I am doing well. 我很好."};
					xmsg.reply(msg.head, body, [], 10/*Prio*/, 8/*Timeout1*/, 0/*Timeout2*/, (err, tkinfo) => {
						if (err) {
							console.error(err);
						} else {
							console.log("  > tkinfo(Reply) id=%s state= %s", tkinfo.id, tkinfo.state);	
						}
					});
				}
				
			});

			
		    // Send message
			var body = {"text": "How are you? 你好嗎?"};
			var files;

			if (testWithFiles) {
				if (isWin32) 
					files = ["D:\\Project\\VOIP\\MoteBus\\Runtime\\*.txt"];
				else
					files = ["/home/richard/Project/MoteBus/Runtime/*.txt"];
			} else {
				files = [];	
			}
			
			function testXMSG(target) {
				console.log('< Send message');
				xmsg.send(target, body, files, 10/*Prio*/, 8/*Timeout1*/, 15/*Timeout2*/, (err, tkinfo) => {
					if (err) {
						console.error(err);
					} else {
						if (tkinfo.state != 'Reply') {
							console.log("  > tkinfo(send) id=%s, state= %s", tkinfo.id, tkinfo.state);	
						} else {
							console.log("  > tkinfo(send) id=%s, state= %s, body=", 
								tkinfo.id, tkinfo.state, tkinfo.msg.body, ", files=", tkinfo.msg.files);
						}
					}
				});
			};

			function testXMSGWithTimer(sn,target,interval) {
				testXMSG(target);
				setTimeout(function(){
					if (sn==gsn)
						testXMSGWithTimer(sn,target,interval);
				},interval);
			}

			//testXMSG('echo@hello.ypcall.com:6788');
			//testXMSG('echo@192.168.0.54');
			//testXMSG('echo@202.153.173.253:6788');
			//testXMSG('echo@127.0.0.1');
			//testXMSG('echo@B85F3168-20B2-4B65-A91B-8AE092798E9B');
			//testXMSGWithTimer(gsn,'echo', 2000);
			//testXMSGWithTimer(gsn,'echo@192.168.0.56', 5000);
			//testXMSG('echo@192.168.1.9');
			//testXMSG('echo@192.168.1.25');
			//testXMSG('echo@192.168.1.26');
			//testXMSG('echo@192.168.0.54');
			//testXMSG('echo@dc');
			testXMSG('echo');
		}
	});
	// ==================================================



	// ================= xRPC API Demo =======================
	var xrpc = motebus.xRPC();

	// Service functions declare
	xrpc.publish( 'app1', {
		"echo": (head, text) => {
			console.log("  app1.echo()");
			return text;
		},

		"hello": (head, msg1, msg2) => {
			console.log("  app1.hello(", msg1, ", ", msg2, ")");

			// Call isolated function
			setTimeout(function(){
				console.log("Now call isolated function ding() -> ", head.from);
				xrpc.call(head.from,"ding",["Time up!"], 10, 8, 10).then((result)=>{
					console.log("  ding() result:", result, "\n");		
				}).catch((err)=>{
					console.log("  ding() error:", err, "\n");		
				});
			},5000);

			return "hi";
		},

		"login": (head, user, pass) => {
			console.log("  app1.login()");
			return true;
		},

		"logout": (head) => {
			console.log("  app1.logout()");
			return true;
		}

	})
	.then((result)=>{
		console.log("xrpc.publish(app1) result:", result, "\n");
	})
	.catch((err)=>{
		console.log("xrpc.publish(app1) error:", err, "\n");
	});


	// Isolated functions declare
	xrpc.isolated({
		"ding": (head, msg)=>{
			console.log("  xrpc.ding() msg =", msg);
			return true;	
		}
	}).then((result)=>{
		console.log("xrpc.isolated() result:", result, "\n");
	}).catch((err)=>{
		console.log("xrpc.isolated() result:", result, "\n");		
	});


	// Call remote function
	function testXRPC(target) {
		xrpc.call( target, "hello", ["test1", "test2"], 10/*Prio*/, 8/*Timeout1*/, 10/*Timeout2*/ )
		.then((result)=>{
			console.log("xrpc.call('hello','test1','test2') result:", result, "\n");
		})
		.catch((err)=>{
			console.log("xrpc.call('hello','test1','test2') error:", err, "\n");
		});
	}


	function testXRPCWithTimer(sn,target,interval) {
		testXRPC(target);
		setTimeout(function(){
			if (sn==gsn)
				testXRPCWithTimer(sn,target,interval);
		},interval);
	}

	//testXRPC("app1@hello.ypcall.com:6788");
	//testXRPC("app1@192.168.0.54");
	//testXRPC("app1@192.168.0.54");
	//testXRPC("app1@127.0.0.1");
	//testXRPCWithTimer(gsn,'app1', 2000);
	//testXRPCWithTimer(gsn,'app1@192.168.1.24:6788', 2000);
	//testXRPC("app1@192.168.0.56");
	//testXRPC("app1@dc");
	testXRPC("app1");

});

motebus.on('off', ()=>{
	console.log("Event: motebus.off()\n");
});


// Monitor host state
motebus.on('hostState', (udid, online) =>{
	console.log("Event: motebus.hostState( udid: %s state: %s )", udid, online?"OnLine":"OffLine" );

	// Get Host Detail Info
	motebus.getHostInfo(udid)
	.then((result)=>{
		console.log("motebus.getHostInfo() result: \n", result, "\n");
	});
});

//motebus.startUp("192.168.1.18", 6060);
motebus.startUp();

