# Method List

## Group api
**CreateGrp**       : Create a group.

**UpdateGrp**       : Update a group.

**RemoveGrp**       : Remove a group (remove node and all child).

**AppendGrp**       : Append in node of tree.

**GetChildGrp**     : Search and get child of node.

**FindGrp**         : Search node.

**FindTreeGrp**     : Search node and get all child and tree inside node.

## Directory Api

**CreateDir**       : Create a directory.

**UpdateDir**       : Update a directory.

**AddGrpToDir**     : Add a group to the directory.

**RemoveGrpFromDir**: Remove a group from the directory.

**FindDir**         : Search directory.

## Schema

### Directory

**name**            :   Required

**insertDate**      :   Default current date

**updateDate**      :   Change after update 

**data**            :   Other properties is dynamic

### Group

**name**            :   Required

**insertDate**      :   Default current date

**updateDate**      :   Change after update 

**data**            :   Other properties is dynamic

**directories**     :   List of directories


## Run Server

for run server type this command : **node index.js**





