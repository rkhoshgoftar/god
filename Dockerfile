FROM node:10.15.0-alpine

LABEL MAINTAINER Reza Khoshgoftar <rkhoshgoftar@gmail.com>

RUN npm install pm2 --global
# add local user for security
RUN addgroup -S god && adduser -S -G god god

USER god

# copy local files into container, set working directory and user
RUN mkdir -p /home/god/app
WORKDIR /home/god/app
COPY . /home/god/app


RUN npm install --production --no-audit

EXPOSE 3030

# ENV PORT=6788 \
#     NAME= \
#     ADDR= 
# COPY ./bin/BusStack_Linux64 /bin/
# EXPOSE ${PORT}

#ENTRYPOINT ["pm2-runtime"]
#CMD  ["./config/pm2.json"]
CMD pm2-runtime ./config/pm2.json 
#&& /bin/mbStack_L64 -p ${PORT} -n ${NAME} -g ${ADDR}
#RUN /home/god/app/config/bin/BusStack_Linux64 -p 6788

#CMD ["pm2-runtime", "./config/pm2.json"]
