const memory = require('feathers-memory');
const error = require('../middleware/error');
const GroupService = require('../services/groupService');
const DirectoryService = require('../services/directoryService');

module.exports = function(app, express){
    app.use('/messages', memory({
        paginate: {
          default: 10,
          max: 25
        }
      }));
      app.use(express.errorHandler());
      app.use('/api/group', new GroupService());
      app.use('/api/directory', new DirectoryService());
      app.use(error);
      
      app.on('connection', connection => app.channel('everybody').join(connection));
      
      app.publish(data => app.channel('everybody'));
}