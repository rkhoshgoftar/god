var exports = module.exports = {};
var MoteChat;
const GroupService = require('../services/directoryService');
const DirectoryService = require('../services/groupService');
const ObjectService = require('../services/objectService');
var group = new GroupService();
var directory = new DirectoryService();
var objects = new ObjectService();
exports.Start = function(web, conf)
{
    MoteChat = require('../infrastructure/usemotechat.js');
    MoteChat.Start(web, conf, function(result){
        console.log('appmain:Start result=%s', JSON.stringify(result));
        if ( result.ErrCode == 0 ){
              MoteChat.Isolated(XrpcMcSecService, function (result) {
                  console.log('motechat isolated: result=%s', JSON.stringify(result));
                  if (result.ErrCode == 0) {
                      MoteChat.OnMessage(InmsgRcve);
                  }
              });
            // MoteChat.Publish(conf.AppName, XrpcMcSecService, function (result) {
            //     console.log('motechat isolated: result=%s', JSON.stringify(result));
            //     if ( result.ErrCode == 0 ){
            //         MoteChat.OnMessage( InmsgRcve );
            //     }
            // });
        } 
    });
}


// Process of Echo
// user send by xmsg
var InmsgRcve = function(ch, inctl, data, cb){
    console.log('%s InmsgRcve: channel=%s, inctl=%s, data=%s', CurrentTime(), ch, JSON.stringify(inctl), JSON.stringify(data));
    var from = inctl.From;
    if ( typeof data.ErrCode == 'undefined' ){    // check if not reply 
        var ret = {"ErrCode":0,"ErrMsg":"OK"};
        if (typeof cb == 'function') {
            console.log('InmsgRcve reply: %s', JSON.stringify(ret));
            cb(ret);   // received, reply OK
        }
        ProcSendEcho(from, data);               // send data back after 200 ms, simulate asyn operation
    }
}

var ProcSendEcho = function(sto, sdata){
    setTimeout(function(topic, to, data){
        console.log('ProcSendEcho topic=%s, to=%s, data=%s', topic, JSON.stringify(to), JSON.stringify(data));
        MoteChat.Send(topic, to, data, null, null);
    }, 200, '', sto, sdata);
}

// user call by xrpc
var XrpcMcSecService = {
    "CreateGrp":async function(head, body){
        console.log("%s xrpc creategrp: body=%s", CurrentTime(), JSON.stringify(body));
        if ( typeof body.data != 'undefined' )
        {
            var Result;
            Result = await group.create(body.data);
            console.log('%s xrpc creategrp: result=%s', CurrentTime(), JSON.stringify(Result));
            return Result;
        } 
        else
           return body;
    },
    "UpdateGrp": async function (head, body) {
        console.log("%s xrpc updategrp: body=%s", CurrentTime(), JSON.stringify(body));
        if (typeof body.data != 'undefined') {
            var Result;
            Result = await group.update(body.data);
            console.log('%s xrpc updategrp: result=%s', CurrentTime(), JSON.stringify(Result));
            return Result;
        } else
            return body;
    },
     "RemoveGrp": async function (head, body) {
         console.log("%s xrpc removegrp: body=%s", CurrentTime(), JSON.stringify(body));
         if (typeof body.data != 'undefined') {
             var Result;
             Result = await group.remove(body.data.condition);
             console.log('%s xrpc removegrp: result=%s', CurrentTime(), JSON.stringify(Result));
             return Result;
         } else
             return body;
     },
     "AppendGrp": async function (head, body) {
         console.log("%s xrpc appendgrp: body=%s", CurrentTime(), JSON.stringify(body));
         if (typeof body.data != 'undefined') {
             var Result;
             Result = await group.append(body.data.condition, body.data.data);
             console.log('%s xrpc appendgrp: result=%s', CurrentTime(), JSON.stringify(Result));
             return Result;
         } else
             return body;
     },
      "GetChildGrp": async function (head, body) {
          console.log("%s xrpc getchildgrp: body=%s", CurrentTime(), JSON.stringify(body));
          if (typeof body.data != 'undefined') {
              var Result;
              Result = await group.getChildren(body.data.condition);
              console.log('%s xrpc getchildgrp: result=%s', CurrentTime(), JSON.stringify(Result));
              return Result;
          } else
              return body;
      },
       "FindGrp": async function (head, body) {
           console.log("%s xrpc findgrp: body=%s", CurrentTime(), JSON.stringify(body));
           if (typeof body.data != 'undefined') {
               var Result;
               Result = await group.find(body.data.condition);
               console.log('%s xrpc findgrp: result=%s', CurrentTime(), JSON.stringify(Result));
               return Result;
           } else
               return body;
       },
        "FindTreeGrp": async function (head, body) {
            console.log("%s xrpc findtreegrp: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await group.findAndGetTree(body.data.condition);
                console.log('%s xrpc findtreegrp: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "GrpAddObj": async function (head, body) {
            console.log("%s xrpc grpaddobj: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await group.addObject(body.data);
                console.log('%s xrpc grpaddobj: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "GrpRemoveObj": async function (head, body) {
            console.log("%s xrpc grpremoveobj: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await group.removeObject(body.data);
                console.log('%s xrpc grpremoveobj: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "GrpObjList": async function (head, body) {
            console.log("%s xrpc grpobjlist: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await group.groupObjectList(body.data);
                console.log('%s xrpc grpobjlist: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "CreateDir": async function (head, body) {
            console.log("%s xrpc createdir: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await directory.create(body.data);
                console.log('%s xrpc createdir: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
         "UpdateDir": async function (head, body) {
             console.log("%s xrpc updatedir: body=%s", CurrentTime(), JSON.stringify(body));
             if (typeof body.data != 'undefined') {
                 var Result;
                 Result = await directory.update(body.data);
                 console.log('%s xrpc updatedir: result=%s', CurrentTime(), JSON.stringify(Result));
                 return Result;
             } else
                 return body;
         },
         "AddGrpToDir": async function (head, body) {
            console.log("%s xrpc addGrpToDir: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await directory.addGroup(body.data);
                console.log('%s xrpc addGrpToDir: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "RemoveGrpFromDir": async function (head, body) {
            console.log("%s xrpc removeGrpFromDir: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await directory.removeGroup(body.data);
                console.log('%s xrpc removeGrpFromDir: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
        "FindDir": async function (head, body) {
               console.log("%s xrpc finddir: body=%s", CurrentTime(), JSON.stringify(body));
               if (typeof body.data != 'undefined') {
                   var Result;
                   Result = await directory.find(body.data.condition);
                   console.log('%s xrpc finddir: result=%s', CurrentTime(), JSON.stringify(Result));
                   return Result;
               } else
                   return body;
           },

        "CreateObj": async function (head, body) {
            console.log("%s xrpc createobj: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await objects.create(body.data);
                console.log('%s xrpc createobj: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        },
         "UpdateObj": async function (head, body) {
             console.log("%s xrpc updateobj: body=%s", CurrentTime(), JSON.stringify(body));
             if (typeof body.data != 'undefined') {
                 var Result;
                 Result = await objects.update(body.data);
                 console.log('%s xrpc updateobj: result=%s', CurrentTime(), JSON.stringify(Result));
                 return Result;
             } else
                 return body;
         },
         "FindObj": async function (head, body) {
            console.log("%s xrpc findobj: body=%s", CurrentTime(), JSON.stringify(body));
            if (typeof body.data != 'undefined') {
                var Result;
                Result = await objects.find(body.data.condition);
                console.log('%s xrpc findobj: result=%s', CurrentTime(), JSON.stringify(Result));
                return Result;
            } else
                return body;
        }

}




var CurrentTime = function(){
    var ret;
    var ct = new Date();
    ret = ct.toLocaleString() + '.' + ct.getMilliseconds().toString();
    return ret;
}