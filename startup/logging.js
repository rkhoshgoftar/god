const winston = require('winston');


module.exports = function(logger){
  
    //winston.add(winston.transports.MongoDB, {db:'mongodb://localhost:27017/god'});

    process.on('uncaughtException', (ex)=>{
        console.log('UnCaught Exception...');
        console.log(ex);
        logger.error(ex.message, ex);
        process.exit(1);
    });

    process.on('unhandledRejection', (ex)=>{
        console.log('Unhandle Rejection...');
        logger.error(ex.message, ex);
        process.exit(1);
    });
}