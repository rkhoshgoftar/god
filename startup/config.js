const socketio = require('@feathersjs/socketio');

module.exports = function(app, express){
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.configure(express.rest());
    app.configure(socketio());
}