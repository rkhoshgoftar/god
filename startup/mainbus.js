var exports = module.exports = {};
const GroupService = require('../services/directoryService');
const DirectoryService = require('../services/groupService');


exports.Main = function(MoteBus,Conf)
{
	Publish(MoteBus,Conf.AppName);
}

function Publish(MoteBus,AppName) {

	MoteBus.on('ready', () => {
        var group = new GroupService();
        var directory = new DirectoryService();
		var xrpc = MoteBus.xRPC();

		xrpc.publish(AppName, {
			"resetreg": async function(params) {
				return params;
			},
			"CreateDir": async function(Head,Msg) {
				var Result;
				Result = await directory.create(Msg);
				return Result;
            },
            "UpdateDir": async function(Head,Msg) {
				var Result;
				Result = await directory.update(Msg);
				return Result;
            },
            "RemoveDir": async function(Head,Msg) {
				var Result;
				Result = await directory.remove(Msg);
				return Result;
            },
            "AppendDir": async function(Head,Msg) {
				var Result;
				Result = await directory.append(Msg.condition, Msg.data);
				return Result;
            },
            "GetChildDir": async function(Head,Msg) {
				var Result;
				Result = await directory.getChildren(Msg);
				return Result;
            },
            "FindDir": async function(Head,Msg) {
				var Result;
				Result = await directory.find(Msg);
				return Result;
            },
            "FindTreeDir": async function(Head,Msg) {
				var Result;
				Result = await directory.findAndGetTree(Msg);
				return Result;
            },
            "CreateGrp": async function(Head,Msg) {
				var Result;
				Result = await group.create(Msg);
				return Result;
            },
            "UpdateGrp": async function(Head,Msg) {
				var Result;
				Result = await group.update(Msg);
				return Result;
            },
            "FindGrp": async function(Head,Msg) {
				var Result;
				Result = await group.find(Msg);
				return Result;
            }
		})
		.then((result)=>{
			console.log("Publish %s App OK.",AppName);
		})
		.catch((err)=>{			
			console.log("Publish %s App Failure.",AppName);
			console.error(err)
		});	

	});
	
}