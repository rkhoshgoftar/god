const mongoose = require('mongoose');
const config = require('config');
module.exports = function(logger) {
  mongoose.connect(config.get('db'))
    .then(() => logger.info('Connected to MongoDB...'));
}