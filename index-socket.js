const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const pty = require('pty.js');

const app = express();
app.use("/",express.static("./"));

const options = {
    key: fs.readFileSync('keys/key.pem'),
    cert: fs.readFileSync('keys/cert.pem')
};

const server = http.createServer(app).listen(8080);
//const server = https.createServer(options, app).listen(8080);

const io = require('socket.io')(server);

// When a new socket connects
io.on('connection', function(socket){
    // Create terminal
    var term = pty.spawn('sh', [], {
       name: 'xterm-color',
       cols: 80,
       rows: 30,
       cwd: process.env.HOME,
       env: process.env
    });
    // Listen on the terminal for output and send it to the client
    term.on('data', function(data){
       socket.emit('output', data);
    });
    // Listen on the client and send any input to the terminal
    socket.on('input', function(data){
       term.write(data);
    });
    // When socket disconnects, destroy the terminal
    socket.on("disconnect", function(){
       term.destroy();
       console.log("bye");
    });
});

