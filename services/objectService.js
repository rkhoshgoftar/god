const objectRepository = require('../repository/objectRepository');
const {changeCondition } = require('../infrastructure/condition');

class ObjectService {
    constructor(){

    }

    async find(param){
        changeCondition(param, 'oid');
        return objectRepository.objectList(param);
    }

    async create(param){
        return objectRepository.createObject(param);
    }

    async update(param){
        changeCondition(param, 'oid');
        return objectRepository.updateObject(param);
    }

    async remove(param){
        changeCondition(param, 'oid');
        return objectRepository.removeObject(param);
    }

    async append(condition, param){
        changeCondition(condition, 'oid');
        return objectRepository.appendObject(condition, param);
    }

    async getChildren(condition){
        changeCondition(condition, 'oid');
        return objectRepository.getChildren(condition);
    }

    async findAndGetTree(condition){
        changeCondition(condition, 'oid');
        return objectRepository.getTree(condition);
    }
}

module.exports = ObjectService;