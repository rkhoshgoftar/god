const groupRepository = require('../repository/groupRepository');
const {changeCondition } = require('../infrastructure/condition');
class GroupService {
    constructor(){

    }

    async find(param){
        changeCondition(param, 'gid');
        return groupRepository.groupList(param);
    }

    async create(param){
        return groupRepository.createGroup(param);
    }

    async update(param){
        changeCondition(param, 'gid');
        return groupRepository.updateGroup(param);
    }

    async remove(param){
        changeCondition(param, 'gid');
        return groupRepository.removeGroup(param);
    }

    async append(condition, param){
        changeCondition(condition, 'gid');
        return groupRepository.appendGroup(condition, param);
    }

    async getChildren(condition){
        changeCondition(condition, 'gid');
        return groupRepository.getChildren(condition);
    }

    async findAndGetTree(condition){
        changeCondition(condition, 'gid');
        return groupRepository.getTree(condition);
    }

    async addObject(param){
        changeCondition(param, 'gid');
        return groupRepository.addObject(param);
    }

    async removeObject(param){
        changeCondition(param, 'gid');
        return groupRepository.removeObject(param);
    }

    async groupObjectList(param){
        changeCondition(param, 'gid');
        return groupRepository.groupObjectList(param);
    }
    
}

module.exports = GroupService;