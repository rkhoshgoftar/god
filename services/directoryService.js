const directoryRepository = require('../repository/directoryRepository');

class DirectoryService {
    constructor(){

    }

    async find(param){
        return directoryRepository.directoryList(param);
    }

    async create(param){
        return directoryRepository.createDirectory(param);
    }

    
    async addGroup(param){
        return directoryRepository.addGroup(param);
    }

    async removeGroup(param){
        return directoryRepository.removeGroup(param);
    }

    async update(param){
        return directoryRepository.updateDirectory(param);
    }
}

module.exports = DirectoryService;