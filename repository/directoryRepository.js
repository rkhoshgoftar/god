const mongoose = require('mongoose');
const {Directory, validateDirectory} = require('../models/directory');
const {success, error} = require('../infrastructure/response');
const _ = require('lodash');

async function createDirectory(param){
    // var {error} = validateDirectory(param);
    // console.log(error);
    // if (error)
    //     throw new Error(error.details[0].message)
   
  try{
        const directory = new Directory(param);
        let find = await Directory.findOne({name : param.name}).select({name: 1});
        if(find)
            return error('name is already exist', -600);

        let result = await directory.save();  
        console.log(result);
        return success(result);    
    }catch(err){
        return error(err.message, -500);
    }   
   
};

async function updateDirectory(param){
   try{
        const result = await Directory.findByIdAndUpdate(param._id,{
            $set:{
                name : param.name,
                updateDate: Date.now(),
                properties: param.properties
            } 
        });
        return success(result); 
   }catch(err){
        return error(err.message, -500);
   }
}

async function addGroup(param){
  try{
        let find = await Directory.findOne({name : param.name});
        if(!find)
            return error('directory not found', -404);
        if (find.groups)
            find.groups.push(param.groupId);
        find.groups = [param.groupId];
        find.updateDate= Date.now();
        let result = await find.save();  
        console.log(result);
        return success(result);    
    }catch(err){
        return error(err.message, -500);
    }   
};

async function removeGroup(param){
    try{
          let find = await Directory.findOne({name : param.name});
          if(!find)
              return error('group not found', -404);
          if (!find.groups)
              return error('groups list is empty', -404);
          if (!find.groups.find(item => item == param.groupId))
              return error('group not found', -404);  
              
            let result =await Directory.update({name : param.name}, {
                $pullAll: {
                    groups : [param.groupId]
                }
            })
          console.log(result);
          return success(result);    
      }catch(err){
          return error(err.message, -500);
      }   
  };


async function directoryList(param){
   
    try{
        const result =await Directory.find(param);
        return success(result); 
    }catch(err){
        return error(err.message, -500);
    }
}

module.exports = {
    createDirectory,
    updateDirectory,
    addGroup,
    removeGroup,
    directoryList
}