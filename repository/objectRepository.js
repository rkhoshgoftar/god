const mongoose = require('mongoose');
const {Objects, validateObject} = require('../models/object');
const {success, error} = require('../infrastructure/response');
const _ = require('lodash');

async function createObject(param){
   
  try{
        const objects = new Objects(param);
        let find = await Objects.findOne({name : param.name}).select({name: 1});
        if(find)
            return error('name is already exist', -600);

        let result = await objects.save();  
        return success(result);    
    }catch(err){
        return error(err.message, -500);
    }   
   
};

async function updateObject(param){
   try{
        const result = await Objects.findByIdAndUpdate(param.oid,{
            $set:{
                name : param.name,
                updateDate: Date.now(),
                properties: param.properties
            } 
        });
        return success(result); 
   }catch(err){
        return error(err.message, -500);
   }
}

async function removeObject(condition){
        
    try{
             let result = await Objects.Remove(condition);  
             return success(result);       
    }      
    catch(err){
              return error(err.message, -500);
    } 
}

async function appendObject(condition, param){
        
    try{
              const objects = new Objects(param);
              let find = await Objects.findOne(condition);
              if(!find)
                      return error('item not found', -404);
              
              find.updateDate= Date.now();
              let result = await find.appendChild(objects);  
              return success(result);  
    } 
    catch(err){
              return error(err.message, -500);
    }       
      
}


async function getChildren(condition){
     try{
              let objects = await Objects.findOne(condition);
              let result = await objects.getChildren();
              return success(result);  
     } 
     catch(err){
               return error(err.message, -500);
     }       
}

async function getTree(condition){
     try{
              let objects = await Objects.findOne(condition);
              let result = await objects.getTree();
              return success(result);        
     } catch(err){
              return error(err.message, -500);
     }
}



async function objectList(param){
   
    try{
        const result =await Objects.find(param);
        return success(result); 
    }catch(err){
        return error(err.message, -500);
    }
}

module.exports = {
    createObject,
    updateObject,
    removeObject,
    appendObject,
    getChildren,
    getTree,
    objectList
}