const mongoose = require('mongoose');
const {Group, validateGroup} = require('../models/group');
const {success, error} = require('../infrastructure/response');

async function createGroup(param){
        
        try{
                const group = new Group(param);
                let find = await Group.findOne({name : param.name}).select({name: 1});
                if(find)
                        return error('name is already exist', -600);

                let result = await group.save();  
                return success(result);          
        }
        catch(err){
                return error(err.message, -500);
        }
        
};

async function updateGroup(param){
        
        try{
                let group = await Group.findById(param.gid);
                if(!group)
                        return error('item not found', -404);
        
                group.name = param.name;
                group.updateDate= Date.now();
                group.properties= param.properties;
                let result = await group.save();  
                return success(result);            
        }  
        catch(err){
                return error(err.message, -500);
        }
}

async function removeGroup(condition){
        
       try{
                let result = await Group.Remove(condition);  
                return success(result);       
       }      
       catch(err){
                 return error(err.message, -500);
       } 
}


async function appendGroup(condition, param){
        
      try{
                const group = new Group(param);
                let find = await Group.findOne(condition);
                if(!find)
                        return error('item not found', -404);
                
                find.updateDate= Date.now();
                let result = await find.appendChild(group);  
                return success(result);  
      } 
      catch(err){
                return error(err.message, -500);
      }       
        
}


async function getChildren(condition){
       try{
                let group = await Group.findOne(condition);
                let result = await group.getChildren();
                return success(result);  
       } 
       catch(err){
                 return error(err.message, -500);
       }       
}

async function getTree(condition){
       try{
                let group = await Group.findOne(condition);
                let result = await group.getTree();
                return success(result);        
       } catch(err){
                return error(err.message, -500);
       }
}

async function addObject(param){
        try{
                let find = await Group.findOne({name : param.name});
                if(!find)
                        return error('Group not found', -404);
                if (find.objects)
                        find.objects.push(param.objectId);
                else
                        find.objects = [param.objectId];

                find.updateDate= Date.now();
                let result = await find.save();  
                return success(result);    
        }catch(err){
                return error(err.message, -500);
        }   
};
      
async function removeObject(param){
        try{
                let find = await Group.findOne({name : param.name});
                if(!find)
                        return error('group not found', -404);
                if (!find.objects)
                        return error('objects list is empty', -404);
                if (!find.objects.find(item => item == param.objectId))
                        return error('object not found', -404);  
                        
                let result =await Group.update({name : param.name}, {
                $pullAll: {
                        objects : [param.objectId]
                }
                })
                return success(result);    
        }catch(err){
                return error(err.message, -500);
        }   
};

async function groupObjectList(param){
        try{
                let find = await Group.findOne({name : param.name});
                if(!find)
                        return error('group not found', -404);
                if (!find.objects)
                        return error('objects list is empty', -404);
                let result = find.objects;
                return success(result);    
        }catch(err){
                return error(err.message, -500);
        }   
};

module.exports = {
        createGroup,
        updateGroup,
        removeGroup,
        appendGroup,
        getChildren,
        getTree,
        addObject,
        removeObject,
        groupObjectList
    };