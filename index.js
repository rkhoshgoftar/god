const winston = require('winston');
require('winston-mongodb');
require('express-async-errors');
const express = require('express');
const app = express();
const bus = require('./startup/mainbus');
const MoteBus = require('motebus');
const chat = require('./startup/mainchat');
var fs = require('fs');
var config = require('config');
var MoteBusConf = JSON.parse(fs.readFileSync(__dirname +'/config/default.json', 'utf8'));
var MoteChatConf = JSON.parse(fs.readFileSync(__dirname +'/config/config.json', 'utf8'));
const logger = winston.createLogger({
    transports: [
        new winston.transports.MongoDB({
            db: config.get('db'),
            collection: 'log',
            storeHost: true,
            capped: true,
        })
    ]
  });

require('./startup/logging')(logger);
require('./startup/db')(logger);

//bus.Main(MoteBus,MoteBusConf);
if (process.env.AppName) MoteChatConf.AppName = process.env.AppName;
if (process.env.DCenter) MoteChatConf.DCenter = process.env.DCenter;
if (process.env.IOC) MoteChatConf.IOC = process.env.IOC;
chat.Start(null,MoteChatConf);

// Start the server
app.listen(3030).on('listening', () =>
  winston.info('Feathers server listening on localhost:3030')
);